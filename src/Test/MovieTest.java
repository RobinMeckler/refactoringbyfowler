package Test;

import Fowler.Customer;
import Fowler.Movie;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;


public class MovieTest {

    Movie testMovie;

    @Before
    public void setUp() throws Exception {
        testMovie = new Movie("Pokemon", 5);
    }

    @After
    public void tearDown() throws Exception {
        testMovie = null;
    }

    @Test
    public void testGetMovie(){
        assertEquals("Pokemon",testMovie.getTitle());
    }

    @Test
    public void testGetPriceCode(){
        assertEquals(5, testMovie.getPriceCode());
        assertNotEquals(0, testMovie.getPriceCode());
    }

    @Test
    public void testSetPriceCode(){
        testMovie.setPriceCode(20);
        assertEquals(20, testMovie.getPriceCode());
    }

    @Test
    public void getPriceCode() {
    }

    @Test
    public void setPriceCode() {
    }

    @Test
    public void getTitle() {
    }
}
