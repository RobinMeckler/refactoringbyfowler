package Test;

import Fowler.Movie;
import Fowler.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RentalTest {

    private Movie testMovie;
    private Rental testRental;

    @Before
    public void setUp() throws Exception {
        testMovie = new Movie( "Iron Man", 10);
        testRental = new Rental(testMovie, 25);
    }

    @After
    public void tearDown() throws Exception {
        testMovie = null;
        testRental = null;
    }

    @Test
    public void testGetMovie() {
        assertEquals(testMovie, testRental.getMovie());
    }

    @Test
    public void testGetDaysRented(){
        assertEquals(25, testRental.getDaysRented());
    }
}
