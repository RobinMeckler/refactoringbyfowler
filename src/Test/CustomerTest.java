package Test;

import Fowler.Movie;
import Fowler.Rental;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import Fowler.Customer;

import static org.junit.Assert.*;

public class CustomerTest {

    Customer testCustomer;
    Rental firstRental;
    Rental secondRental;


    @Before
    public void setUp() throws Exception {
        firstRental = new Rental(new Movie("movie1", 1), 10);
        secondRental = new Rental(new Movie("movie2", 2), 5);
        testCustomer = new Customer("Rick");
        testCustomer.addRental(firstRental);
        testCustomer.addRental(secondRental);

    }

    @After
    public void tearDown() throws Exception {
        testCustomer = null;
    }

    @Test
    public void addRental() {
        Rental rental = new Rental(new Movie("Test movie", 0), 3);
        testCustomer.addRental(rental);
        //assertEquals(1,testCustomer.rentals.size());
    }

    @Test
    public void getName() {
        assertEquals("Rick", testCustomer.getName());
    }

    @Test
    public void statement() {
        assertTrue(testCustomer.statement().startsWith("Rental Record for Rick"));
        assertTrue(testCustomer.statement().endsWith(" frequent renter points"));
    }
}